import React from 'react';
import './Table.css';

class Add extends React.Component {
  handleSubmit = (e) => {
    e.preventDefault()
    const requestOptions = {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({id:e.target.id.value, title:e.target.title.value, author:e.target.author.value, category:e.target.category.value })
    };
    fetch("https://i7xtbamtbj.execute-api.eu-west-3.amazonaws.com/books", requestOptions)
      .then(()=>window.location.reload());
  }

  render() {
  return (
    <div>
        <form onSubmit={this.handleSubmit}>
          <input type="text" name="id" placeholder="Id"/>
          <input type="text" name="title" placeholder="Title"/>
          <input type="text" name="author" placeholder="Author"/>
          <input type="text" name="category" placeholder="Category"/>
          <button type="submit">Submit</button>
        </form>
    </div>
    );
  }
}

export default Add;