import React from 'react';
import './Table.css';

class Table extends React.Component {
  constructor(){
    super()
    this.state = {
      book:[],
      inEditMode: {status:"false", rowId:null}
    }
    this.edit=this.edit.bind(this);
    this.remove=this.remove.bind(this);
  }
  
  fetchBooks(){
    fetch("https://i7xtbamtbj.execute-api.eu-west-3.amazonaws.com/books")
        .then(response => response.json())
        .then(data => {    
          this.setState({book: data.Items})
        })
        .catch(e => {
            console.log(e);
        });
    }
  
  componentDidMount(){
    this.fetchBooks();
  }

  edit(id,e){
    e.preventDefault();
    this.setState({inEditMode:{status:true,rowId:id}});   
  }

  editDone(id,e){
    e.preventDefault();  
    var author=document.getElementById("author").textContent;
    var category=document.getElementById("category").textContent;
    var title=document.getElementById("name").textContent;
    const requestOptions = {
      method: 'PUT',
      body: JSON.stringify({id:id, title:title, author:author, category:category})
    };
    fetch("https://i7xtbamtbj.execute-api.eu-west-3.amazonaws.com/books", requestOptions)
      .then(()=>this.fetchBooks())
      .then(this.setState({inEditMode:{status:false,rowId:null}}));
  }
  
  remove(id,e){
      e.preventDefault();
      var str="https://i7xtbamtbj.execute-api.eu-west-3.amazonaws.com/books";
      fetch(str.concat('/',id), { method: 'DELETE' })
        .then(()=>this.fetchBooks());
  }
  
  renderTableData(){
    var i=this.state.book;
    return i.map((book,index)=> {
        const { id, title, author, category } = book;
        return <tbody>
          {(this.state.inEditMode.status && this.state.inEditMode.rowId===id) ? (
            <tr key={id}>
            <td>{id}</td>
            <td contentEditable='true' id='name'>{title}</td>
            <td contentEditable='true' id='author'>{author}</td>
            <td contentEditable='true' id='category'>{category}</td>
            <td><button name="save" id={id} onClick={(e) => this.editDone(id, e)}>Save</button></td>
            </tr>
          ): ( 
            <tr key={id}>
              <td>{id}</td>
          <td>{title}</td>
          <td>{author}</td>
          <td>{category}</td>
          <td><button name="edit" id={id} onClick={(e) => this.edit(id, e)}></button></td>
          <td><button name="remove" id={id} onClick={(e) => this.remove(id, e)}></button></td>
          </tr>)
          }
          </tbody>
    })
  }
    
  renderTableHeader() {
    return <thead><tr><th>ID</th><th>TITLE</th><th>AUTHOR</th><th>CATEGORY</th><th>EDIT</th><th>REMOVE</th></tr></thead>
  }

  render() {
    return (
       <div>
          <h1 id='title'>Books</h1>
             <table class="center">
             {this.renderTableHeader()}
             {this.renderTableData()}
             </table>     
       </div>
    )
  }
}

export default Table;
