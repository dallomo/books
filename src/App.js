import './App.css';
import React from 'react';
import Add from './components/Add';
import Table from './components/Table';

function App() {
  return (
    <div className="App">
      <Table />
      <Add />
    </div>
  );
}

export default App;
